# Regolith On Stage Website

[![pipeline status](https://gitlab.com/regolith-on-stage/regolith-on-stage.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/regolith-on-stage/regolith-on-stage.gitlab.io/-/commits/master)

This website was built with Designmodo's [Startup](https://designmodo.com/startup/) application. It was then converted to a [Hugo](https://gohugo.io/) site in order to ease the maintenance.
